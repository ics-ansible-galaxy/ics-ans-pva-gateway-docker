import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('pva_gateway_docker')


def test_pvagw_running(host):
    with host.sudo():
        cmd = host.command("docker logs pvagw")
    assert cmd.rc == 0
    assert "Starting pvagw" in cmd.stdout
